# NodeJS Tutorial

## Overview

* **Easiest way to run a node server:** Sensible defaults & includes everything you need with minimal setup.
* **Compatible:** All browsers.

`tipper` is based on the following libraries & tools:

* [`express`](https://github.com/expressjs/express): Performant, extensible web server framework

* ES6 typings
* Extensible via Express middlewares
* Accepts `application/json` content-types
* Runs everywhere: Can be deployed via `now`, `up`, AWS Lambda, Heroku etc.
* Supports middleware out of the box.

## Getting Started
***
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- Install **Node LTS 12**
  - Download: https://nodejs.org/en/download/


### Installation

  Install yarn as global packages if not ``` npm install -g yarn```
```
    yarn install || npm i
```
*You can use npm. But I recommend usage yarn instend of npm*

### Usage
***

#### Running server application

```
  yarn start || npm run start
```

Server will start with port http://localhost:3000

#### Running the tests

## Deployment

To deploy your application server with [Heroku](https://heroku.com), follow these instructions:

1.  Download and install the [Heroku Command Line Interface](https://devcenter.heroku.com/articles/heroku-cli#download-and-install) (previously Heroku Toolbelt)
2.  Log in to the Heroku CLI with `heroku authorized`
3.  Navigate to the root directory of your `server` server
4.  Create the Heroku instance by executing `heroku create`
5.  Deploy your server server by executing `git push heroku master`

### `up` (Coming soon 🔜 )

### AWS Lambda (Coming soon 🔜 )